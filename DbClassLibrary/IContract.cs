﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using DbClassLibrary.Entities;

namespace DbClassLibrary
{
    [ServiceContract]
    public interface IContract
    {
        [OperationContract]       
        List<Car> GetCars(TopCars topCars);

        [OperationContract]
        List<Car> GetAllCars();

        [OperationContract]
        List<TopCars> GetAllTopCars();

        [OperationContract]
        List<Car> GetAllCarsToPrice(int price);


    }
}
