﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DbClassLibrary.Entities;

namespace DbClassLibrary.Tables
{
 public   class TableTopCars
    {
        private string connectionString;

        public TableTopCars(string connectionString)
        {
            this.connectionString = connectionString;

        }

        public List<TopCars> GetAllTopCars()
        {
            List<TopCars> topCars = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = "Select * From `topcars`";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        topCars = new List<TopCars>();

                        while (reader.Read())
                        {
                            TopCars top = new TopCars()
                            {
                                Id = reader.GetInt32("id"),
                                CarBrand = reader.GetString("carBrand")
                            };


                            topCars.Add(top);
                        }
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return topCars;
        }
        public void UpdateTopCars(string name, int id)
        {
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"UPDATE `topcars` SET `carBrand`='{name}' WHERE `id`={id};";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }


        }

        public void DeleteTopCars(int id)
        {
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"DELETE FROM `topcars` WHERE `id`={id};";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }


        }

        public void ClearTopCars()
        {
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = "Truncate Table `topcars`";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

        }

        public void InsertTopCar(TopCars topCars)
        {

            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"INSERT INTO `topcars` (`carBrand`) VALUES ('{topCars.CarBrand}');";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }
        }
    }
}
