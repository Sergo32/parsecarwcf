﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using DbClassLibrary;
namespace WCF_Server
{
    public partial class Form1 : Form
    {
        private ServiceHost host = null;
        public Form1()
        {
            InitializeComponent();
        }
        
        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (host == null)
            {
                host = new ServiceHost(typeof(ClassAction));
                host.Open();// слушает входящие соеденение
                richTextBoxInfo.Text += "Сервер стартанул !!! " + DateTime.Now + Environment.NewLine; ;
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (host != null)
            {
                // Завершение ожидания прихода сообщений.
                host.Close();
                host = null;

                richTextBoxInfo.Text += "\n Сервер остановлен.    " + DateTime.Now + Environment.NewLine;
            }
        }
    }
}
