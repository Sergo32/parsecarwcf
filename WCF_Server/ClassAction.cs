﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbClassLibrary;
using DbClassLibrary.Entities;

namespace WCF_Server
{
    class ClassAction : IContract
    {
        public List<Car> GetAllCars()
        {
            return DbManager.GetInstance().TableCar.GetAllCars();
        }

        public List<Car> GetAllCarsToPrice(int price)
        {
            return DbManager.GetInstance().TableCar.GetAllCarsToPrice(price);
        }

        public List<TopCars> GetAllTopCars()
        {

            return DbManager.GetInstance().TableTopCars.GetAllTopCars();
        }

        public List<Car> GetCars(TopCars topCars)
        {
            return DbManager.GetInstance().TableCar.GetAllCarsIdMark(topCars.Id);
        }
    }
}
