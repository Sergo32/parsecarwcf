﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using DbClassLibrary.Entities;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;
using System.ServiceModel;
using DbClassLibrary;


namespace WCF_Client
{
    class ControllerFormMain
    {
        private FormMain form;

        public ControllerFormMain(FormMain form)
        {
            this.form = form;
        }

        public void FillComboBoxTopCar()
        {
            ChannelFactory<IContract> channelFactory = new ChannelFactory<IContract>("ServerEndpoint");
            IContract contract = channelFactory.CreateChannel();

            List<TopCars> topCars = contract.GetAllTopCars();

            form.comboBoxTopCar.DataSource = null;
            form.comboBoxTopCar.DataSource = topCars;
        }

        public void ClearFields()
        {
            form.comboBoxTopCar.SelectedItem = -1;
        }

        public void GetCarsById()
        {
            TopCars top = (TopCars)form.comboBoxTopCar.SelectedItem;

            ChannelFactory<IContract> channelFactory = new ChannelFactory<IContract>("ServerEndpoint");
            IContract contract = channelFactory.CreateChannel();

            List<Car> cars = contract.GetCars(top);

            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = cars;
            form.dataGridViewCars.Columns["TopCars"].Visible = false;
            form.dataGridViewCars.Columns["IdCarBrand"].Visible = false;
        }

        public void GetAllCars()
        {

            ChannelFactory<IContract> channelFactory = new ChannelFactory<IContract>("ServerEndpoint");
            IContract contract = channelFactory.CreateChannel();

            List<Car> cars = contract.GetAllCars();

            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = cars;
            form.dataGridViewCars.Columns["TopCars"].Visible = false;
        }

        public void GetAllCarsToPrice()
        {
            if (form.maskedTextBoxPrice.Text == "")
            {
                MessageBox.Show("Введите значение");
                return;
            }
            int price = int.Parse(form.maskedTextBoxPrice.Text);
            ChannelFactory<IContract> channelFactory = new ChannelFactory<IContract>("ServerEndpoint");
            IContract contract = channelFactory.CreateChannel();

            List<Car> cars = contract.GetAllCarsToPrice(price);

            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = cars;
            form.dataGridViewCars.Columns["TopCars"].Visible = false;
            form.dataGridViewCars.Columns["IdCarBrand"].Visible = false;
        }
    }
}
